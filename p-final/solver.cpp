/* compute optimal solutions for sliding block puzzle. */
#include <SDL2/SDL.h>
#include <stdio.h>
#include <cstdlib>   /* for atexit() */
#include <algorithm>
using std::swap;
#include <cassert>
#include <vector>
using std::vector;
#include <iostream>
using std::cout;
#include <queue>
using std::queue;
#include <map>
using std::map;
//#include <SDL2/SDL_ttf.h>
/* SDL reference: https://wiki.libsdl.org/CategoryAPI */

/* initial size; will be set to screen size after window creation. */
int SCREEN_WIDTH = 640;
int SCREEN_HEIGHT = 480;
int fcount = 0;
int mousestate = 0;
SDL_Point lastm = {0,0}; /* last mouse coords */
SDL_Point cp_lastm = {0,0}; 	// make a copy of the last mouse coords
SDL_Rect bframe; /* bounding rectangle of board */
static const int ep = 2; /* epsilon offset from grid lines */

bool init(); /* setup SDL */
void initBlocks();

//#define FULLSCREEN_FLAG SDL_WINDOW_FULLSCREEN_DESKTOP
 #define FULLSCREEN_FLAG 0

/* NOTE: ssq == "small square", lsq == "large square" */
enum bType {hor,ver,ssq,lsq};
struct block {
	SDL_Rect R; /* screen coords + dimensions */
	bType type; /* shape + orientation */
	/* TODO: you might want to add other useful information to
	 * this struct, like where it is attached on the board.
	 * (Alternatively, you could just compute this from R.x and R.y,
	 * but it might be convenient to store it directly.) */
	int number ;    // number of the block (1,2,3,4).
	int position; 	// position of the block in the board 0-19
	int move;
	void rotate() /* rotate rectangular pieces */
	{
		if (type != hor && type != ver) return;
		type = (type==hor)?ver:hor;
		number = (type ==hor)?1:2;
		swap(R.w,R.h);
	}
};

#define NBLOCKS 10
block B[NBLOCKS];
block* dragged = NULL;

block* findBlock(int x, int y);
void close(); /* call this at end of main loop to free SDL resources */
SDL_Window* gWindow = 0; /* main window */
SDL_Renderer* gRenderer = 0;
//SDL_Surface* surface = 0;  // for having an image filling the rects.
//SDL_Texture* texture = 0;  // for having an image filling the rects.
 

vector<int> init_board = {1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};  // 0 means empty.
map<uint64_t, vector<int>> hash_blocks;  // to link every board with the location of the blocks.
vector<int> current_board;  // the current board that we trying to get the next generation for it.
vector<int> next_board;     // the next generation of the current board.
uint64_t current_num;  		// 64-bit num represent the current board.
uint64_t next_num;    		// 64-bit num represent the next-gen board.
vector<int> ecells(2,-1);   // array to hold the location of the empty cells of each board.
bool Goal = false;   		// flag when we reach the goal.
uint64_t goal_num;  		// 64-bit number of the final board.
size_t arrow = 0;   		// to navigate the solutions. 
vector<uint64_t> path;  	// array to hold the stages of the solution.
bool play_mood = false; 	// flag for the play-mood.
bool auto_mood = false; 	// flag for the auto-mood.
uint64_t pprevious_num; 	// use it with the play-mood.
vector<uint64_t> ppath; 	// to hold the stages of the play-mood.
bool flag = false;  		// check if i have two empty cells next to each other



bool init()
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_Init failed.  Error: %s\n", SDL_GetError());
		return false;
	}
	/* NOTE: take this out if you have issues, say in a virtualized
	 * environment: */
	if(!SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1")) {
		printf("Warning: vsync hint didn't work.\n");
	}
	/* create main window */
	gWindow = SDL_CreateWindow("Sliding block puzzle solver",
								SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								SCREEN_WIDTH, SCREEN_HEIGHT,
								SDL_WINDOW_SHOWN|FULLSCREEN_FLAG);
	if(!gWindow) {
		printf("Failed to create main window. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	/* set width and height */
	SDL_GetWindowSize(gWindow, &SCREEN_WIDTH, &SCREEN_HEIGHT);
	/* setup renderer with frame-sync'd drawing: */
	gRenderer = SDL_CreateRenderer(gWindow, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(!gRenderer) {
		printf("Failed to create renderer. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	SDL_SetRenderDrawBlendMode(gRenderer,SDL_BLENDMODE_BLEND);

	initBlocks();

/*	surface = SDL_LoadBMP("/home/csc103/csc212/csc212-projects/csc212-final/p-final/img/try_img.bmp");
	texture = SDL_CreateTextureFromSurface(gRenderer,surface);
	SDL_FreeSurface(surface);    */

	return true;
}
/* TODO: you'll probably want a function that takes a state / configuration
 * and arranges the blocks in accord.  This will be useful for stepping
 * through a solution.  Be careful to ensure your underlying representation
 * stays in sync with what's drawn on the screen... */
void initBlocks()
{
	int& W = SCREEN_WIDTH; 					//640
	int& H = SCREEN_HEIGHT; 				//480
	int h = H*3/4;  						//360
	int w = 4*h/5; 							//288
	int u = h/5-2*ep; 						//68
	int mw = (W-w)/2; 						//176
	int mh = (H-h)/2; 						//60

	/* setup bounding rectangle of the board: */
	bframe.x = (W-w)/2; 					//176
	bframe.y = (H-h)/2; 					//60
	bframe.w = w; 							//288
	bframe.h = h; 							//360

	/* NOTE: there is a tacit assumption that should probably be
	 * made explicit: blocks 0--4 are the rectangles, 5-8 are small
	 * squares, and 9 is the big square.  This is assumed by the
	 * drawBlocks function below. */

	for (size_t i = 0; i < 5; i++) {
		B[i].R.x = (mw-2*u)/2; 				//20
		B[i].R.y = mh + (i+1)*(u/5) + i*u;  //73.6
		B[i].R.w = 2*(u+ep); 				//140
		B[i].R.h = u; 						//68
		B[i].type = hor;
		B[i].number = 1;    				// it's horizontal 
		B[i].position = -1;   				// position is: outside the board
	}
	B[4].R.x = mw+ep;   					// this's the rect inside the board 
	B[4].R.y = mh+ep;
	B[4].R.w = 2*(u+ep);
	B[4].R.h = u;
	B[4].type = hor;
	B[4].number = 1;
	B[4].position = 0;
	/* small squares */
	for (size_t i = 0; i < 4; i++) {
		B[i+5].R.x = (W+w)/2 + (mw-2*u)/2 + (i%2)*(u+u/5);
		B[i+5].R.y = mh + ((i/2)+1)*(u/5) + (i/2)*u;
		B[i+5].R.w = u;
		B[i+5].R.h = u;
		B[i+5].type = ssq;
		B[i+5].number = 3;    				//  it's ssq.
		B[i+5].position = -1;
	}
	B[9].R.x = B[5].R.x + u/10;
	B[9].R.y = B[7].R.y + u + 2*u/5;
	B[9].R.w = 2*(u+ep);
	B[9].R.h = 2*(u+ep);
	B[9].type = lsq;
	B[9].number = 4; 						//it's lsq. 
	B[9].position = -1;   					// position is: outside the board
}
void drawBlocks()
{
	/* rectangles */
	SDL_SetRenderDrawColor(gRenderer, 0x43, 0x4c, 0x5e, 0xff);
	for (size_t i = 0; i < 5; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* small squares */
	SDL_SetRenderDrawColor(gRenderer, 0x5e, 0x81, 0xac, 0xff);
	for (size_t i = 5; i < 9; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* large square */
	if(B[9].position == 13)
		SDL_SetRenderDrawColor(gRenderer, 34, 150, 34, 0xff);
	else
		SDL_SetRenderDrawColor(gRenderer, 0xa3, 0xbe, 0x8c, 0xff);
	SDL_RenderFillRect(gRenderer,&B[9].R);

//	SDL_SetRenderDrawColor(gRenderer, 0,0,0,255);
//	SDL_RenderClear(gRenderer);
//	SDL_RenderCopy(gRenderer,texture,NULL,&B[9].R);
//	SDL_RenderPresent(gRenderer);
//	}

}
/* return a block containing (x,y), or NULL if none exists. */
block* findBlock(int x, int y)
{
	/* NOTE: we go backwards to be compatible with z-order */
	for (int i = NBLOCKS-1; i >= 0; i--) {
		if (B[i].R.x <= x && x <= B[i].R.x + B[i].R.w &&
				B[i].R.y <= y && y <= B[i].R.y + B[i].R.h)
			return (B+i);
	}
	return NULL;
}
void close()
{
	SDL_DestroyRenderer(gRenderer); gRenderer = NULL;
	SDL_DestroyWindow(gWindow); gWindow = NULL;

//	SDL_DestroyTexture(texture); texture = NULL;

	SDL_Quit();
}
void render()
{
	/* draw entire screen to be black: */
	SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xff);
	SDL_RenderClear(gRenderer);

	/* first, draw the frame: */
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int w = bframe.w;
	int h = bframe.h;
	SDL_SetRenderDrawColor(gRenderer, 0x39, 0x39, 0x39, 0xff);
	SDL_RenderDrawRect(gRenderer, &bframe);
	/* make a double frame */
	SDL_Rect rframe(bframe);
	int e = 3;
	rframe.x -= e; 
	rframe.y -= e;
	rframe.w += 2*e;
	rframe.h += 2*e;
	SDL_RenderDrawRect(gRenderer, &rframe);

	/* draw some grid lines: */
	SDL_Point p1,p2;
	SDL_SetRenderDrawColor(gRenderer, 0x19, 0x19, 0x1a, 0xff);
	/* vertical */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x;
	p2.y = p1.y + h;
	for (size_t i = 1; i < 4; i++) {
		p1.x += w/4;
		p2.x += w/4;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	/* horizontal */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x + w;
	p2.y = p1.y;
	for (size_t i = 1; i < 5; i++) {
		p1.y += h/5;
		p2.y += h/5;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	SDL_SetRenderDrawColor(gRenderer, 0xd8, 0xde, 0xe9, 0x7f);
	SDL_Rect goal = {bframe.x + w/4 + ep, bframe.y + 3*h/5 + ep,
	                 w/2 - 2*ep, 2*h/5 - 2*ep};
	SDL_RenderDrawRect(gRenderer,&goal);

	/* now iterate through and draw the blocks */
	drawBlocks();
	/* finally render contents on screen, which should happen once every
	 * vsync for the display */
	SDL_RenderPresent(gRenderer);
}


void remove_old(block* b,int prev_position)
{
	if(prev_position != -1){
		init_board[prev_position] = 0;
		if(b->type == hor)
			init_board[prev_position+1] = 0;
		else if(b->type == ver)
			init_board[prev_position+4] = 0;
		else if( b->type == lsq){
			init_board[prev_position+1] = 0;
			init_board[prev_position+4] = 0;
			init_board[prev_position+5] = 0;
		}
	}
}
void add_old(block* b, int prev_position)
{
	if(prev_position != -1){
		init_board[prev_position] = b->number;
		if(b->type == hor)
			init_board[prev_position+1] = b->number;
		else if(b->type == ver)
			init_board[prev_position+4] = b->number;
		else if( b->type == lsq){
			init_board[prev_position+1] = b->number;
			init_board[prev_position+4] = b->number;
			init_board[prev_position+5] = b->number;
		}
	}
}
void add_to_board(block* b, int new_x, int new_y,int prev_position)
{
	if(b->position == -1) return ;
	int i = b->position;
	
	if(b->type == hor){
		if(play_mood){
			if( i != 3 && i != 7 && i != 11 && i != 15 && i != 19 && init_board[i+1] ==0 && !(i == prev_position-3 || i == prev_position+5 || i == prev_position-5 || i == prev_position+3 || ( i == prev_position-2 && new_y != cp_lastm.y ) || (i == prev_position+2 && new_y != cp_lastm.y) )  ){ 
				init_board[i]   = b->number;  		// update the new position
				init_board[i+1] = b->number;  		// update the new position
				b->R.x = new_x; 		// snap it to nearest position in the board
				b->R.y = new_y;  		// snap it to neatest position in the board
				return ;
			}else { 
				b->position = prev_position;
				b->R.x = cp_lastm.x;  	//snap it to a rand position outside
				b->R.y = cp_lastm.y;  	//snap it to a rand position outside
				add_old(b, prev_position);
				return ;
			}
		}else{
			if( i != 3 && i != 7 && i != 11 && i != 15 && i != 19 && init_board[i+1] ==0 ){ 
				init_board[i]   = b->number;  		// update the new position
				init_board[i+1] = b->number;  		// update the new position
				b->R.x = new_x; 		// snap it to nearest position in the board
				b->R.y = new_y;  		// snap it to neatest position in the board
				return ;
			}else { 
				b->position = prev_position;
				b->R.x = cp_lastm.x;  	//snap it to a rand position outside
				b->R.y = cp_lastm.y;  	//snap it to a rand position outside
				add_old(b, prev_position);
				return ;
			}
		}
	}else if(b->type == ver){
		if(play_mood){
			if(i != 16 && i != 17 && i != 18 && i != 19 && init_board[i+4] ==0 && !( i == prev_position-5 || i == prev_position-3 || i == prev_position+3 || i == prev_position+5 || ( i == prev_position+1 && new_y != cp_lastm.y ) || (i == prev_position-1 && new_y != cp_lastm.y ) ) ){ 
				init_board[i]   = b->number;
				init_board[i+4] = b->number;
				b->R.x = new_x; 
				b->R.y = new_y;
				return ;
			}else {
				b->position = prev_position;
				b->R.x = cp_lastm.x;  	//snap it to a rand position outside
				b->R.y = cp_lastm.y;  	//snap it to a rand position outside
				add_old(b, prev_position);
				return ;
			}
		}
		if(i != 16 && i != 17 && i != 18 && i != 19 && init_board[i+4] ==0){ 
			init_board[i]   = b->number;
			init_board[i+4] = b->number;
			b->R.x = new_x; 
			b->R.y = new_y;
			return ;
		}else {
			b->position = prev_position;
			b->R.x = cp_lastm.x;  	//snap it to a rand position outside
			b->R.y = cp_lastm.y;  	//snap it to a rand position outside
			add_old(b, prev_position);
			return ;
		}
	}else if(b->type == lsq){
		if(i != 3 && i != 7 && i != 11 && i != 15 && i != 16 && i != 17 && i != 18 && i != 19 && init_board[i+1]==0 && init_board[i+4]==0 && init_board[i+5]==0 ){
			init_board[i]   = b->number;
			init_board[i+1] = b->number;
			init_board[i+4] = b->number;
			init_board[i+5] = b->number;
			b->R.x = new_x; 
			b->R.y = new_y;
			return ;
		}else {
			b->position = prev_position;
			b->R.x = cp_lastm.x; 			//snap it to a rand position outside
			b->R.y = cp_lastm.y;  			//snap it to a rand position outside
			add_old(b, prev_position);
			return ;
		}	
	}else { // ssq
		if(play_mood){
			if( init_board[i] == 0 && (  ((i==prev_position-1||i==prev_position+1)&&new_y==cp_lastm.y) || (i==prev_position-2&&new_y==cp_lastm.y&&init_board[prev_position-1]==0) || (i==prev_position+2&&new_y==cp_lastm.y&&init_board[prev_position+1]==0) || ((i==prev_position-4||i==prev_position+4)&&new_x==cp_lastm.x) || (i==prev_position-8&&new_x==cp_lastm.x&&init_board[prev_position-4]==0) || (i==prev_position+8&&new_x==cp_lastm.x&&init_board[prev_position+4]==0) || (i==prev_position-5&&(init_board[prev_position-4]==0||init_board[prev_position-1]==0)&&(prev_position!=0&&prev_position!=4&&prev_position!=8&&prev_position!=12&&prev_position!=16)) || (i==prev_position-3&&(init_board[prev_position-4]==0||init_board[prev_position+1]==0)&&(prev_position!=3&&prev_position!=7&&prev_position!=11&&prev_position!=15&&prev_position!=19)) || (i==prev_position+3&&(init_board[prev_position+4]==0||init_board[prev_position-1]==0)&&(prev_position!=0&&prev_position!=4&&prev_position!=8&&prev_position!=12&&prev_position!=16)) || (i==prev_position+5&&(init_board[prev_position+4]==0||init_board[prev_position+1]==0)&&(prev_position!=3&&prev_position!=7&&prev_position!=11&&prev_position!=15&&prev_position!=19))   ) ){
					init_board[i] = b->number;
					b->R.x = new_x;
					b->R.y = new_y;
			}else{
				b->position = prev_position;
				b->R.x = cp_lastm.x;
				b->R.y = cp_lastm.y;
				add_old(b, prev_position);
				return;
			}
		}else{
			init_board[i] = b->number;
			b->R.x = new_x;
			b->R.y = new_y;
		}
	}
}
void remove_from_board(block* b)
{
	int i = b->position;
	if(i != -1){
		init_board[i] = 0;
		if(b->type == hor)
			init_board[i+1] = 0;
		else if(b->type == ver)
			init_board[i+4] = 0;
		else if( b->type == lsq){
			init_board[i+1] = 0;
			init_board[i+4] = 0;
			init_board[i+5] = 0;
		}
	}
	b->position = -1;
}
void snap(block* b)
{
	/* TODO: once you have established a representation for configurations,
	 * you should update this function to make sure the configuration is
	 * updated when blocks are placed on the board, or taken off.  */
	assert(b != NULL);
	/* upper left of grid element (i,j) will be at
	 * bframe.{x,y} + (j*bframe.w/4,i*bframe.h/5) */
	/* translate the corner of the bounding box of the board to (0,0). */
	int x = b->R.x - bframe.x;
	int y = b->R.y - bframe.y;
	int uw = bframe.w/4;
	int uh = bframe.h/5;
	/* NOTE: in a perfect world, the above would be equal. */
	int i = (y)/uh; /* row */
	int j = (x)/uw; /* col */
	int prev_position = b->position;
	if (0 <= i && i < 5 && 0 <= j && j < 4) {
		b->position = 4*i+j;  	// update the block's position inside the board
		remove_old(b, prev_position);
		if( init_board[b->position] == 0){  		// if the position is empty
			int b_R_new_x = bframe.x + j*uw + ep;
			int b_R_new_y = bframe.y + i*uh + ep; 
			add_to_board(b, b_R_new_x, b_R_new_y,prev_position);
		}else{   					//if there's another block in that position 
			b->position = prev_position;
			b->R.x = cp_lastm.x; 			//snap it to it's previous position outside
			b->R.y = cp_lastm.y;  			//snap it to it's privious position outside
			add_old(b,prev_position);
		}
	}
}

bool is_board(const vector<int>& V)
{
	int empty = 0, rec = 0, ssq = 0, lsq = 0;
	for(size_t i = 0; i < V.size(); i++){
		switch ( V[i] ){
			case 0:
				empty++;
				break;
			case 1:
			case 2:
				rec++;
				break;
			case 3:
				ssq++;
				break;
			case 4:
				lsq++;
				break;
		}
	}
	if( empty == 2 && rec == 10 && ssq == 4 && lsq == 4 ) return true;
	return false;
}
uint64_t board_to_num(const vector<int>& V)
{
	uint64_t result = 0;
	for(size_t i =0; i < V.size(); i++){
		result += ( V[i] * pow(5,i) );
	}
	return result;
}
vector<int> num_to_board(uint64_t num)
{
	vector<int> V (20,0);
	for(size_t i =0; i<20;i++){
		V[i] = num % 5;
		num /= 5;
	}
	return V;
}
/* return the location of the empty cells in the board  */
void empty_cells(const vector<int>& V)
{
	int ecount = 0;
	ecells[0] = -1;
	ecells[1] = -1;
	for(size_t i =0; i < V.size(); i++){
		if(V[i] == 0){
			ecount++;
			if(ecells[0] == -1) ecells[0] = i;
			else ecells[1] = i;
		}
	}
	if( ecount != 2) cout <<" we have error in the empty cell function \n";
}
/* check if the block is a neighbor of an empty cell is the init_board */
bool is_neighbor(block* P, const vector<int>& p_current_board)
{
	assert(b != NULL);
	empty_cells(p_current_board);
	for(size_t empty_cell = 0; empty_cell < ecells.size(); empty_cell++){
		switch ( P->type ){
			case hor:
				if( (P->position == ecells[empty_cell]+1) || (P->position == ecells[empty_cell]-2) || (P->position == ecells[empty_cell]+4) || (P->position == ecells[empty_cell]+3) || (P->position == ecells[empty_cell]-4) || (P->position == ecells[empty_cell]-5) ) return true;
				break;
			case ver:
				if( (P->position == ecells[empty_cell]+4) || (P->position == ecells[empty_cell]-8) || (P->position == ecells[empty_cell]+1) || (P->position == ecells[empty_cell]-3) || (P->position == ecells[empty_cell]-1) || (P->position == ecells[empty_cell]-5) ) return true;
				break;
			case ssq:
				if( (P->position == ecells[empty_cell]+4) || (P->position == ecells[empty_cell]-4) || (P->position == ecells[empty_cell]+1) || (P->position == ecells[empty_cell]-1) ) return true;
				break;
			case lsq:
				if( (P->position == ecells[empty_cell]+4) || (P->position == ecells[empty_cell]+3) || (P->position == ecells[empty_cell]-8) || (P->position == ecells[empty_cell]-9) || (P->position == ecells[empty_cell]+1) || (P->position == ecells[empty_cell]-3) || (P->position == ecells[empty_cell]-2) || (P->position == ecells[empty_cell]-6) ) return true;
				break;
		}
	}
	return false;
}
void set_blocks_in_position(const vector<int>& V)
{
	vector<int> current_block_pos = hash_blocks[board_to_num(V)];
	for(size_t n = 0; n<current_block_pos.size(); n++){
		B[n].position = current_block_pos[n];
		int i = B[n].position>>2 ,j = B[n].position-(i<<2);
		int x = bframe.x + j*bframe.w/4 + ep;
		int y = bframe.y + i*bframe.h/5 + ep;
		B[n].R.x = x;
		B[n].R.y = y;
	}
}
vector<int> current_blocksV()
{
	vector<int> V;
	for(size_t i=0; i <10; i++){
		V.push_back(B[i].position);
	}
	return V;
}

void block_mup(block* b, int e_cell)
{
	next_board = current_board;
	int i = e_cell>>2 ,j = e_cell-(i<<2);
	int x = bframe.x + j*bframe.w/4 + ep;
	int y = bframe.y + i*bframe.h/5 + ep; 
	int p = b->position;
	int e_cell2 = e_cell-4;
	switch (b->type){
		case hor:
			if(p == e_cell+4 && next_board[e_cell+1] == 0){
				next_board[p]   = 0;
				next_board[p+1] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell+1] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
			}
			if(p == e_cell+3 && next_board[e_cell-1] == 0){
				next_board[p]   = 0;
				next_board[p+1] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell-1] = b->number;
				b->position = p-4;
				b->R.x = bframe.x + (j-1)*bframe.w/4 + ep;
				b->R.y = y; 
			}
			break;
		case ver:
			if( (e_cell2 >= 0 && e_cell2 <= 7) && (next_board[e_cell2] == 0) && !flag ){
				next_board[p]   = 0;
				next_board[p+4] = 0;
				next_board[e_cell]  = b->number;
				next_board[e_cell2] = b->number;
				b->position = e_cell2;
				b->R.x = x;
				b->R.y = bframe.y + (i-1)*bframe.h/5 + ep; 
				if(ecells[0] == ecells[1]-4) flag = true;
			}else{  
				next_board[p+4]   = 0;
				next_board[e_cell] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
				if(flag) flag = false;
			}
			break;
		case ssq:
			if( (e_cell2 >= 0 && e_cell2 <= 11 )&&( next_board[e_cell2] == 0)&& !flag ){
				next_board[p] = 0;
				next_board[e_cell2] = b->number;
				b->position = e_cell2;
				b->R.x = x;
				b->R.y = bframe.y + (i-1)*bframe.h/5 + ep; 
				if(ecells[0] == ecells[1]-4) flag = true;
			}else{		
				next_board[p]   = 0;
				next_board[e_cell] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
				if(flag) flag = false;
			}
			break;
		case lsq:
			if(p == e_cell+4 && next_board[e_cell+1] == 0){
				next_board[p+4] = 0;
				next_board[p+5] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell+1] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y; 
			}
			if(p == e_cell+3 && next_board[e_cell-1] == 0){
				next_board[p+4] = 0;
				next_board[p+5] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell-1] = b->number;
				b->position = e_cell-1;
				b->R.x = bframe.x + (j-1)*bframe.w/4 + ep;
				b->R.y = y; 
			}
			break;
	}
}
void block_mdown(block* b, int e_cell)
{
	next_board = current_board;
	int i = e_cell>>2 ,j = e_cell-(i<<2);
	int x = bframe.x + j*bframe.w/4 + ep;
	int y = bframe.y + i*bframe.h/5 + ep; 
	int p = b->position;
	int e_cell2 = e_cell+4;
	switch (b->type){
		case hor:
			if(p == e_cell-4 && next_board[e_cell+1] == 0){
				next_board[p]   = 0;
				next_board[p+1] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell+1] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
			}
			if(p == e_cell-5 && next_board[e_cell-1] == 0){
				next_board[p]   = 0;
				next_board[p+1] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell-1] = b->number;
				b->position = p+4;
				b->R.x = bframe.x + (j-1)*bframe.w/4 + ep;
				b->R.y = y; 
			}
			break;
		case ver:
			if( (e_cell2 >= 12 && e_cell2 <=19) && (next_board[e_cell2] == 0) && !flag ){
				next_board[p]   = 0;
				next_board[p+4] = 0;
				next_board[e_cell]  = b->number;
				next_board[e_cell2] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y; 
				if(ecells[0] == ecells[1]-4) flag = true;
			}else{   
				next_board[p]   = 0;
				next_board[e_cell] = b->number;
				b->position = p+4;
				b->R.x = x;
				b->R.y = bframe.y + (i-1)*bframe.h/5 + ep;
				if(flag) flag = false;
			}
			break;
		case ssq:
			if ( (e_cell2 >= 8 && e_cell2 <= 19) && (next_board[e_cell2] == 0) && !flag ){
				next_board[p] = 0;
				next_board[e_cell2] = b->number;
				b->position = e_cell2;
				b->R.x = x;
				b->R.y = bframe.y + (i+1)*bframe.h/5 + ep;
				if(ecells[0] == ecells[1]-4) flag = true;
			}else{
				next_board[p]   = 0;
				next_board[e_cell] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
				if(flag) flag = false;
			}
			break;
		case lsq:
			if(p == e_cell-8 && next_board[e_cell+1] == 0){
				next_board[p]   = 0;
				next_board[p+1] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell+1] = b->number;
				b->position = p+4;
				b->R.x = x;
				b->R.y = bframe.y + (i-1)*bframe.h/5 + ep;
			}
			if(p == e_cell-9 && next_board[e_cell-1] == 0){
				next_board[p]   = 0;
				next_board[p+1] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell-1] = b->number;
				b->position = p+4;
				b->R.x = bframe.x + (j-1)*bframe.w/4 + ep;
				b->R.y = bframe.y + (i-1)*bframe.h/5 + ep;
			}
			break;
	}	
}
void block_mleft(block* b, int e_cell)
{
	next_board = current_board;
	int i = e_cell>>2 ,j = e_cell-(i<<2);
	int x = bframe.x + j*bframe.w/4 + ep;
	int y = bframe.y + i*bframe.h/5 + ep; 
	int p = b->position;
	int e_cell2 = e_cell -1;
	switch (b->type){
		case hor: 
			if( (e_cell2 == 0 || e_cell2 == 4 || e_cell2 == 8 || e_cell2 == 12 || e_cell2 == 16) && (next_board[e_cell2] == 0) && !flag){
				next_board[p]   = 0;
				next_board[p+1] = 0;
				next_board[e_cell2] = b->number;
				next_board[e_cell]  = b->number;
				b->position = e_cell2;
				b->R.x = bframe.x + (j-1)*bframe.w/4 + ep;
				b->R.y = y;
				if(ecells[0] == ecells[1]-1) flag = true;
			}else{  
				next_board[p+1] = 0;
				next_board[e_cell] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
				if(flag) flag = false;
			}
			break;
		case ver:
			if(p == e_cell+1 && next_board[e_cell+4] == 0){
				next_board[p]   = 0;
				next_board[p+4] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell+4] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
			}
			if(p == e_cell-3 && next_board[e_cell-4] == 0){
				next_board[p]   = 0;
				next_board[p+4] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell-4] = b->number;
				b->position = p-1;
				b->R.x = x;
				b->R.y = bframe.y + (i-1)*bframe.h/5 + ep;   
			}
			break;
		case ssq:
			if( (e_cell2 == 0 || e_cell2 == 4 || e_cell2 == 8 || e_cell2 == 12 || e_cell2 == 16 || e_cell2 == 1 || e_cell2 == 5 || e_cell2 == 9 || e_cell2 == 13 || e_cell2 == 17 )&&( next_board[e_cell2] == 0) && !flag ){
				next_board[p] = 0;
				next_board[e_cell2] = b->number;
				b->position = e_cell2;
				b->R.x = bframe.x + (j-1)*bframe.w/4 + ep;
				b->R.y = y; 
				if(ecells[0] == ecells[1]-1) flag = true;
			}else{
				next_board[p]   = 0;
				next_board[e_cell] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
				if(flag) flag =false;
			}
			break;
		case lsq:
			if(p == e_cell+1 && next_board[e_cell+4] == 0){
				next_board[p+1] = 0;
				next_board[p+5] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell+4] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y; 
			}
			if(p == e_cell-3 && next_board[e_cell-4] == 0){
				next_board[p+1] = 0;
				next_board[p+5] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell-4] = b->number;
				b->position = p-1;
				b->R.x = x;
				b->R.y = bframe.y + (i-1)*bframe.h/5 + ep;    
			}
			break;
	}
}
void block_mright(block* b, int e_cell)
{
	next_board = current_board;
	int i = e_cell>>2 ,j = e_cell-(i<<2);
	int x = bframe.x + j*bframe.w/4 + ep;
	int y = bframe.y + i*bframe.h/5 + ep; 
	int p = b->position;
	int e_cell2 = e_cell+1;
	switch (b->type){
		case hor:
			if( (e_cell2 == 3 || e_cell2 == 7 || e_cell2 == 11 || e_cell2 == 15 || e_cell2 == 19 ) && (next_board[e_cell2] == 0) && !flag ){
				next_board[p]   = 0;
				next_board[p+1] = 0;
				next_board[e_cell]  = b->number;
				next_board[e_cell2] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
				if(ecells[0] == ecells[1]-1) flag = true;
			}else{   
				next_board[p] = 0;
				next_board[e_cell] = b->number;
				b->position = p+1;
				b->R.x = bframe.x + (j-1)*bframe.w/4 + ep;
				b->R.y = y;
				if(flag) flag = false;
			}
			break;
		case ver:
			if(p == e_cell-1 && next_board[e_cell+4] == 0){
				next_board[p]   = 0;
				next_board[p+4] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell+4] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
			}
			if(p == e_cell-5 && next_board[e_cell-4] == 0){
				next_board[p]   = 0;
				next_board[p+4] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell-4] = b->number;
				b->position = p+1;
				b->R.x = x;
				b->R.y = bframe.y + (i-1)*bframe.h/5 + ep;   
			}
			break;
		case ssq:
			if( (e_cell2 == 2 || e_cell2 == 3 || e_cell2 == 6 || e_cell2 == 7 || e_cell2 == 10 || e_cell2 == 11 || e_cell2 == 14 || e_cell2 == 15 || e_cell2 == 18 || e_cell2 == 19 )&& (next_board[e_cell2] == 0) && !flag ){
				next_board[p] = 0;
				next_board[e_cell2] = b->number;
				b->position = e_cell2;
				b->R.x = bframe.x + (j+1)*bframe.w/4 + ep;
				b->R.y = y;
				if(ecells[0] == ecells[1]-1) flag = true;
			}else{
				next_board[p]   = 0;
				next_board[e_cell] = b->number;
				b->position = e_cell;
				b->R.x = x;
				b->R.y = y;
				if(flag) flag = false;
			}
			break;
		case lsq:
			if(p == e_cell-2 && next_board[e_cell+4] == 0){
				next_board[p]   = 0;
				next_board[p+4] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell+4] = b->number;
				b->position = p+1;
				b->R.x = bframe.x + (j-1)*bframe.w/4 + ep;
				b->R.y = y; 
			}
			if(p == e_cell-6 && next_board[e_cell-4] == 0){
				next_board[p]   = 0;
				next_board[p+4] = 0;
				next_board[e_cell]   = b->number;
				next_board[e_cell-4] = b->number;
				b->position = p+1;
				b->R.x = bframe.x + (j-1)*bframe.w/4 + ep;
				b->R.y = bframe.y + (i-1)*bframe.h/5 + ep;     
			}
			break;
	}
}

bool is_goal(const vector<int>& V)
{
	if(V[13] == 4 && V[14] ==4 && V[17] == 4 && V[18] == 4)
		return true;
	else
		return false;
}
void next_gen(int empty_cell, queue<vector<int>>& boards, map<uint64_t,uint64_t>& hash )
{
	next_board = current_board;   
	if(!is_board(next_board))
		cout << "something went wrong in next gen\n";
	block* nbr[4];  	// neighbor to the empty cell {up, down, left, right}
	int I = empty_cell>>2 ,J = empty_cell-(I<<2);
	int x = (bframe.x + J*bframe.w/4 + 2*ep)+(bframe.w/8);
	int y = (bframe.y + I*bframe.h/5 + 2*ep)+(bframe.h/10); 

	nbr[0] = findBlock(x, (y-bframe.h/5) );  // up
	nbr[1] = findBlock(x, (y+bframe.h/5) );  // down
	nbr[2] = findBlock( (x-bframe.w/4), y );  // left
	nbr[3] = findBlock( (x+bframe.w/4), y);  // right

	if(nbr[0]){
		do{
			block_mdown(nbr[0], empty_cell); // move the block above the empty cell DOWN
			uint64_t next_num = board_to_num(next_board);
			if(is_board(next_board)){
				if(hash.find(next_num) == hash.end()){
					boards.push(next_board);
					hash[next_num] = current_num; // parent of the board, for the path
					hash_blocks[next_num] = current_blocksV();
					Goal = is_goal(next_board);
					if(Goal){
						goal_num    = next_num;
						return;
					}
				}
			}else {
				cout << " something was wrong while i'm moving a " << nbr[0]->number << " block down \n";
			}
			set_blocks_in_position(current_board);
		}while(flag);
	}

	if(nbr[1]){
		do{
			next_board = current_board;   // move down
			if(!is_board(next_board)) 
				cout << "something went wrong in next gen\n";
			block_mup(nbr[1], empty_cell);
			uint64_t next_num = board_to_num(next_board);
			if(is_board(next_board)){
				if(hash.find(next_num) == hash.end()){
					boards.push(next_board);	
					hash[next_num] = current_num; // parent of the board, for the path 
					hash_blocks[next_num] = current_blocksV();
					Goal = is_goal(next_board);
					if(Goal){
						goal_num    = next_num;
						return;
					}
				}
			}else {
				cout << " something was wrong while i'm moving a "<< nbr[1]->number << " block up \n";
			}
			set_blocks_in_position(current_board);
		}while(flag);
	}

	if(nbr[2]){
		do{
			next_board = current_board;   // move down
			if(!is_board(next_board)) 
				cout << "something went wrong in next gen\n";
			block_mright(nbr[2], empty_cell);
			uint64_t next_num = board_to_num(next_board);
			if(is_board(next_board)){
				if(hash.find(next_num) == hash.end()){
					boards.push(next_board);	
					hash[next_num] = current_num; // parent of the board, for the path 
					hash_blocks[next_num] = current_blocksV();
					Goal = is_goal(next_board);
					if(Goal){
						goal_num    = next_num;
						return;
					}
				}
			}else {
				cout << " something was wrong while i'm moving a " << nbr[2]->number << " block right \n";
			}
			set_blocks_in_position(current_board);
		}while(flag);
	}

	if(nbr[3]){
		do{
			next_board = current_board;   // move down
			if(!is_board(next_board)) 
				cout << "something went wrong in next gen\n";
			block_mleft(nbr[3], empty_cell);
			uint64_t next_num = board_to_num(next_board);
			if(is_board(next_board)){
				if(hash.find(next_num) == hash.end()){
					boards.push(next_board);	
					hash[next_num] = current_num; // parent of the board, for the path 
					hash_blocks[next_num] = current_blocksV();
					Goal = is_goal(next_board);
					if(Goal){
						goal_num    = next_num;
						return;
					}
				}
			}else {
				cout << " something was wrong while i'm moving a "<<nbr[3]->number << " block left \n";
			}
			set_blocks_in_position(current_board);
		}while(flag);
	}
}

int main(int argc, char *argv[])
{
	/* TODO: add option to specify starting state from cmd line? */
	/* start SDL; create window and such: */
	
	cout<<"\n\n\t\t*** Welcome to sliding block puzzle ***\n";
	cout<<"My name is THE GREAT GOOSE and magically i'll guide you today to the land of fun.\n";
	cout<<"To arrange the blocks inside the board, i casted a magical spell on the blocks, just place \n";
	cout<<"the upper left corner of a block anywere inside an empty cell and it'll snap in place.\n";
	cout<<"I have two moods:\n\nthe first mood called \"The Sorcerer Mood\" which will activate \n";
	cout<<"my magical powers to find and solve your puzzle \"if such solution exist of course :D\". \n";
	cout<<"Press s to activate The Sorcerer Mood and use the left and the right arrows to navigate \n";
	cout<<"through the solution steps.\n\n";
	cout<<"The second mood is called \"The Sorcerer's apprentice\" which is for you \n";
	cout<<"young apprentice to practice your magic.\n";
	cout<<"Press p to activate the Apprentice  Mood, if you need my assistance just press s and i'll\n";
	cout<<"solve it for you then you can see my solution by using the left and the right arrows\n";
	cout<<"Also you can continue your practice from any step you want.\n";
	cout<<"If you lift a block from it's position but you don't know where to put it, \n";
	cout<<"don't worry just leave it in the same position and i'll not count that against you :)\n";
	cout <<"\t\t\t*** Now let the fun begin ***\n\n\n"; 
	
	
	if(!init()) {
		printf( "Failed to initialize from main().\n" );
		return 1;
	}
	atexit(close);
	bool quit = false; /* set this to exit main loop. */
	SDL_Event e;
	/* main loop: */
	while(!quit) {
		/* handle events */
		while(SDL_PollEvent(&e) != 0) {
			/* meta-q in i3, for example: */
			if(e.type == SDL_MOUSEMOTION) {
				if (mousestate == 1 && dragged) {
					int dx = e.button.x - lastm.x;
					int dy = e.button.y - lastm.y;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged->R.x += dx;
					dragged->R.y += dy;
				}
			} else if (e.type == SDL_MOUSEBUTTONDOWN) {
				if (e.button.button == SDL_BUTTON_RIGHT) {
					block* b = findBlock(e.button.x,e.button.y);
					// NOTE: rotate only if the block is outside the board
					if (b && b->position == -1) b->rotate();    
				} else {
					mousestate = 1;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged = findBlock(e.button.x,e.button.y);
					if(dragged){
						// make a copy of the block coordinates before moving
						cp_lastm.x = dragged->R.x; 			 
						cp_lastm.y = dragged->R.y;  		
						pprevious_num = board_to_num(init_board);
					}
				}
				/* XXX happens if during a drag, someone presses yet
				 * another mouse button??  Probably we should ignore it. */
			} else if (e.type == SDL_MOUSEBUTTONUP) {
				if (e.button.button == SDL_BUTTON_LEFT) {
					mousestate = 0;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					if (dragged) {
						if(Goal) Goal = false;
						if(play_mood){ /* play mood game */
							if(( is_neighbor(dragged, init_board) && dragged->R.x >=bframe.x && dragged->R.x <= (bframe.x+bframe.w)) && ( dragged->R.y >= bframe.y && dragged->R.y <= (bframe.y+bframe.h) )){
								snap(dragged);
								if(is_board(init_board)){
									uint64_t board_num  = board_to_num(init_board);
									ppath.push_back(board_num);
									hash_blocks[board_num] = current_blocksV();
									Goal = is_goal(init_board);
									if(Goal){ 
										path.clear();
										for(size_t i =0; i < ppath.size();i++){
											if(ppath[i] != ppath[i+1])
												path.push_back(ppath[i]);
										}
										arrow = path.size()-1;
										cout << "Well done young apprentice :D , You reached your goal in : "  << path.size()-1 << " steps.\n";
										play_mood = false ;
										auto_mood = false ;
										cout<< "** Apprentice Mood is off ** \n";
									}
								}else {
									cout << " something was wrong ";
								}
							}else{	/* invalid move for the dragged in the play mood */
								dragged->R.x = cp_lastm.x; 			//snap it to it's previous position 
								dragged->R.y = cp_lastm.y;  		//snap it to it's privious position  
							}
						}else{
							/* Original mood game
							 * snap to grid if nearest location is empty. */
							if(( dragged->R.x >=bframe.x && dragged->R.x <= (bframe.x+bframe.w)) && ( dragged->R.y >= bframe.y && dragged->R.y <= (bframe.y+bframe.h) )){
								snap(dragged);
							}else{	
								remove_from_board(dragged);
							}
						}
					}
					dragged = NULL;
				}
			} else if (e.type == SDL_QUIT) {
				quit = true;
			} else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
					case SDLK_ESCAPE:
					case SDLK_q:
						quit = true;
						break;
					case SDLK_LEFT:
						/* TODO: show previous step of solution */	
						if(Goal){
							if(auto_mood){
								if(arrow <path.size()-1) arrow++;
								if(arrow >=0 && arrow <= path.size()-1){
									set_blocks_in_position(num_to_board(path[arrow]));
									init_board = num_to_board(path[arrow]);
								}
							}
							if(!auto_mood){   // play mood
								if (arrow > 0) arrow--;
								if(arrow >=0 && arrow <= path.size()-1){
									set_blocks_in_position(num_to_board(path[arrow]));
									init_board = num_to_board(path[arrow]);
								}
							}
						}   
						break;
					case SDLK_RIGHT:
						/* TODO: show next step of solution */
						if(Goal){
							if(auto_mood){
								if (arrow > 0) arrow--;
								if(arrow >=0 && arrow <= path.size()-1){
									set_blocks_in_position(num_to_board(path[arrow]));
									init_board = num_to_board(path[arrow]);
								}
							}
							if(!auto_mood){   // play mood
								if(arrow <path.size()-1) arrow++;
								if(arrow >=0 && arrow <= path.size()-1){
									set_blocks_in_position(num_to_board(path[arrow]));
									init_board = num_to_board(path[arrow]);
								} 
							}
						}
						break;
					case SDLK_g:
						/* TODO: print the state to stdout
						 * (maybe for debugging purposes...) */
								
						cout << "\nThe initial board is : \n" ;
						for(size_t i = 0; i < init_board.size();i++){
							cout << init_board[i] <<" ";
						}
						cout << "\nthe blocks positions is :\n";
						for(size_t i = 0; i <10; i++){
							cout << B[i].position << " ";
						}
						break;
					case SDLK_s:
						/* TODO: try to find a solution */
						
						if(is_board(init_board)){
							auto_mood = true;
							ppath.clear();
					//		play_mood = false;
							if(Goal) Goal = false;
							if(is_goal(init_board)){
								cout<<"YaaY :D , I reached my goal in : 0 steps.\n";
								break;
							}
							queue<vector<int>> boards;  		// queue for BFS
							map<uint64_t, uint64_t> hash;  	// hash table to see if we had the current step before or not {num -- num}
							uint64_t init_num = board_to_num(init_board);
							hash[init_num] = 2; // root of the path tree
							hash_blocks[init_num] = current_blocksV(); // to save the location of the blocks in each board
							boards.push(init_board);
							while(!boards.empty() && !Goal){
								current_board = boards.front();
								current_num = board_to_num(current_board);
								set_blocks_in_position(current_board);	
								// if we reached our goal then break
								if(is_goal(current_board)){
									Goal = true;
									break;
									}
								boards.pop();
								// go search for what can be the next steps  
								empty_cells(current_board);
								for(size_t i = 0; i < ecells.size(); i++){
									next_gen(ecells[i], boards, hash);
									if(Goal) break;	
								}
							}
							// to freak the screen on the init_board
							set_blocks_in_position(init_board);
							if(is_goal(next_board)){
						//	if(Goal){ 
								uint64_t i = goal_num;
								// fill the vector path with the solution steps.
								path.clear();
								path.push_back(i);
								while (hash[i] != 2){
									i = hash[i];
									path.push_back(i);
								}
								arrow = path.size()-1;
								cout << "YaaaY :D , I reached my goal in : "  << path.size()-1 << " steps.\n";
							}else{
								cout << "Sorry, I could NOT find any solution for you :( \n";
							}    
						}
						break;
					case SDLK_p:
						/* TODO: to start the play mood */
						if(is_board(init_board)){
							play_mood = true;
							cout<< "** Apprantice Mood is on ** \n";
							auto_mood = false;
							Goal = false;
							uint64_t first_num = board_to_num(init_board);
							ppath.clear();
							ppath.push_back(first_num);
							hash_blocks[first_num] = current_blocksV();
							if(is_goal(init_board)){
								cout << "Well done young apprentice :D , You reached the goal in 0 steps.\n";
								play_mood = false;
								cout<<"** Apprantice Mood is off **\n";
							}
						}
						break;
					default:
						break;
				}
			}
		}
		fcount++;
		render();
	}

	printf("total frames rendered: %i\n",fcount);
//	cout << " num of boards pop out is :" << pop_count << "\n";
	return 0;
}
